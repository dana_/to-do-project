import AddTodo from './components/AddTodo'
import EditTodo from './components/EditTodo'

export const routes = [
  { path: '/', component: AddTodo},
  { path: '/edit', component: EditTodo}
]